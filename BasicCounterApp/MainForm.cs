﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using BasicCounterLib;

namespace BasicCounterApp
{
	public partial class MainForm : Form
	{
		BasicCounter counter = new BasicCounter(0);

		
		public MainForm()
		{
			InitializeComponent();
			string label=string.Format("{0}", counter.getTotal());
			totalLabel.Text=label;
			
		}
		
		void IncrementButtonClick(object sender, EventArgs e)
		{
			counter.incrementCounter();
			string label=string.Format("{0}", counter.getTotal());
			totalLabel.Text=label;
		}
		
		void DecrementButtonClick(object sender, EventArgs e)
		{
			counter.decrementCounter();
			string label=string.Format("{0}", counter.getTotal());
			totalLabel.Text=label;
		}
		
		void ResetButtonClick(object sender, EventArgs e)
		{
			counter.resetCounter();
			string label=string.Format("{0}", counter.getTotal());
			totalLabel.Text=label;
		}
		void TotalTitleClick(object sender, EventArgs e)
		{
	
		}
	}
}
