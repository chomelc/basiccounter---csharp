﻿/*
 * Created by SharpDevelop.
 * User: Clem
 * Date: 25/04/2019
 * Time: 09:28
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace BasicCounterApp
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.Button decrementButton;
		private System.Windows.Forms.Button incrementButton;
		private System.Windows.Forms.Label totalTitle;
		private System.Windows.Forms.Label totalLabel;
		private System.Windows.Forms.Button resetButton;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.decrementButton = new System.Windows.Forms.Button();
			this.incrementButton = new System.Windows.Forms.Button();
			this.totalTitle = new System.Windows.Forms.Label();
			this.totalLabel = new System.Windows.Forms.Label();
			this.resetButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// decrementButton
			// 
			this.decrementButton.BackColor = System.Drawing.SystemColors.ButtonFace;
			this.decrementButton.Font = new System.Drawing.Font("Montserrat Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.decrementButton.Location = new System.Drawing.Point(12, 87);
			this.decrementButton.Name = "decrementButton";
			this.decrementButton.Size = new System.Drawing.Size(149, 68);
			this.decrementButton.TabIndex = 0;
			this.decrementButton.Text = "-";
			this.decrementButton.UseVisualStyleBackColor = false;
			this.decrementButton.Click += new System.EventHandler(this.DecrementButtonClick);
			// 
			// incrementButton
			// 
			this.incrementButton.BackColor = System.Drawing.SystemColors.ButtonFace;
			this.incrementButton.Font = new System.Drawing.Font("Montserrat Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.incrementButton.Location = new System.Drawing.Point(363, 87);
			this.incrementButton.Name = "incrementButton";
			this.incrementButton.Size = new System.Drawing.Size(146, 68);
			this.incrementButton.TabIndex = 1;
			this.incrementButton.Text = "+";
			this.incrementButton.UseVisualStyleBackColor = false;
			this.incrementButton.Click += new System.EventHandler(this.IncrementButtonClick);
			// 
			// totalTitle
			// 
			this.totalTitle.Font = new System.Drawing.Font("Montserrat Light", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.totalTitle.Location = new System.Drawing.Point(216, 43);
			this.totalTitle.Name = "totalTitle";
			this.totalTitle.Size = new System.Drawing.Size(100, 28);
			this.totalTitle.TabIndex = 2;
			this.totalTitle.Text = "TOTAL";
			this.totalTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.totalTitle.Click += new System.EventHandler(this.TotalTitleClick);
			// 
			// totalLabel
			// 
			this.totalLabel.Font = new System.Drawing.Font("Montserrat Light", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.totalLabel.Location = new System.Drawing.Point(216, 71);
			this.totalLabel.Name = "totalLabel";
			this.totalLabel.Size = new System.Drawing.Size(100, 94);
			this.totalLabel.TabIndex = 3;
			this.totalLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// resetButton
			// 
			this.resetButton.BackColor = System.Drawing.Color.Brown;
			this.resetButton.FlatAppearance.BorderColor = System.Drawing.Color.DarkRed;
			this.resetButton.FlatAppearance.BorderSize = 0;
			this.resetButton.Font = new System.Drawing.Font("Montserrat Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.resetButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
			this.resetButton.Location = new System.Drawing.Point(192, 187);
			this.resetButton.Name = "resetButton";
			this.resetButton.Size = new System.Drawing.Size(149, 45);
			this.resetButton.TabIndex = 4;
			this.resetButton.Text = "RAZ";
			this.resetButton.UseVisualStyleBackColor = false;
			this.resetButton.Click += new System.EventHandler(this.ResetButtonClick);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ActiveBorder;
			this.ClientSize = new System.Drawing.Size(521, 244);
			this.Controls.Add(this.resetButton);
			this.Controls.Add(this.totalLabel);
			this.Controls.Add(this.totalTitle);
			this.Controls.Add(this.incrementButton);
			this.Controls.Add(this.decrementButton);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.Name = "MainForm";
			this.Text = "BasicCounterApp";
			this.ResumeLayout(false);

		}
	}
}
