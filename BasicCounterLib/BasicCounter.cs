﻿using System;
using System.Collections.Generic;

namespace BasicCounterLib
{
	public class BasicCounter
	{
		private int total;
		
		// Constructor BasicCounter
		// Constructor of the class BasicCounter
		// IN : initial total of the counter
		// OUT : /		
		public BasicCounter(int total)
		{
			this.total = total;
		}
		
		// Getter getTotal
		// Getter allowing to get the current total of the counter
		// IN : /
		// OUT : current total of the counter
		public int getTotal() {
			return this.total;
		}
		
		// Method incrementCounter
		// Method that allows to increment the current total of the counter
		// IN : /
		// OUT : /
		public void incrementCounter ()
		{
			this.total+=1;
		}
		
		// Method decrementCounter
		// Method that allows to decrement the current total of the counter
		// IN : /
		// OUT : /
		public void decrementCounter ()
		{
			if (this.total>=1) // Handling negative numbers
			{
				this.total-=1;
			}
		}
		
		
		// Method resetCounter
		// Method that allows to reset the total of the counter
		// IN : /
		// OUT : /
		public void resetCounter ()
		{
			this.total=0;
		}
	}
}