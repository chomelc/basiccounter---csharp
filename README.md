A solution for learning base of OOP, Csharp and GUIs with .Net 

## UML Diagramm

![UML diagramm](img/uml.png)


## Projects

BasicCounterSolution is made of three projects

### BasicCounterApp and BasicCounterLib

A dummy GUI application for managing a basic counter

### BasicCounterTest

A dummy Xunit collection of unit tests for learning Unit test !

## Author
> Clemence CHOMEL