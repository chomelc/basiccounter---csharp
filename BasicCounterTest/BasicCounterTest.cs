﻿using System;
using NUnit.Framework;
using BasicCounterLib;

namespace BasicCounterTest
{
	[TestFixture]
	public class BasicCounterTest
	{
		[Test]
		public void TestConstructor()
		{
			BasicCounter counter = new BasicCounter(0);
			Assert.AreEqual(0, counter.getTotal());
			
			BasicCounter counterBis = new BasicCounter(5);
			Assert.AreEqual(5, counterBis.getTotal());
		}
		
		[Test]
		public void TestIncrementCounter()
		{
			BasicCounter counter = new BasicCounter(0);
			Assert.AreEqual(0, counter.getTotal());
			counter.incrementCounter();
			Assert.AreEqual(1, counter.getTotal());
		}
		
		[Test]
		public void TestDecrementCounter()
		{
			BasicCounter counter = new BasicCounter(0);
			Assert.AreEqual(0, counter.getTotal());
			counter.decrementCounter();
			Assert.AreEqual(0, counter.getTotal());
			
			for (int i=0; i<4; i++)
			{
				counter.incrementCounter();
			}			
			Assert.AreEqual(4, counter.getTotal());
			
			counter.decrementCounter();
			Assert.AreEqual(3, counter.getTotal());	
		}
		
		[Test]
		public void TestResetCounter()
		{
			BasicCounter counter = new BasicCounter(0);
			Assert.AreEqual(0, counter.getTotal());
			
			for (int i=0; i<4; i++)
			{
				counter.incrementCounter();
			}			
			Assert.AreEqual(4, counter.getTotal());
			
			counter.resetCounter();
			Assert.AreEqual(0, counter.getTotal());
		}
	}
}
